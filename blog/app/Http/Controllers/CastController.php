<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
#Eloquent
use App\Cast;
// use File;

// class CastController extends Controller
// {
//     public function create(){
//         return view('cast.create');
//     }

//     public function store(Request $request){
//         $request->validate([
//             'nama' => 'required',
//             'umur' => 'required|numeric',
//             'bio' => 'required',
//         ]);

//         DB::table('cast')->insert(
//             ['nama' => $request['nama'],
//              'umur' => $request['umur'],
//              'bio' => $request['bio']]
//         );

//         return redirect('/cast');
//     }

//     public function index(){
//         $cast = DB::table('cast')->get();
//         return view('cast.index', compact('cast'));
//     }

//     public function show($id){
//         $cast = DB::table('cast')->where('id', $id)->first();
//         return view('cast.show', compact('cast'));
//     }

//     public function edit($id){
//         $cast = DB::table('cast')->where('id', $id)->first();
//         return view('cast.edit', compact('cast'));
//     }

//     public function update($id, Request $request){
//         $request->validate([
//             'nama' => 'required',
//             'umur' => 'required|numeric',
//             'bio' => 'required',
//         ]);

//         $affected = DB::table('cast')
//               ->where('id', $id)
//               ->update(
//                     [
//                       'nama' => $request['nama'],
//                       'umur' => $request['umur'],
//                       'bio' => $request['bio'],
//                     ]
//                 );
//         return redirect('/cast');
//     }

//     public function destroy($id){
//         DB::table('cast')->where('id', $id)->delete();
//         return redirect('/cast');
//     }
// }

###     CRUD Eloquent ORM     ###

class CastController extends Controller{
    public function create(){
        $cast = DB::table('cast')->get();
        return view('cast.create', compact('cast'));
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
            // 'thumbnail' => 'image|mines:jpg,png,jpeg'
        ]);

        // $namaThummbnail = time(). '.' . $request->thumbnail->extension();
        // $request->thumbnail->move(public_path('thumbnail'), $namaThummbnail);

        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();

        return redirect('/cast');
    }

    public function index(){
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = Cast::findOrFail($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = Cast::findOrFail($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
            // 'thumbnail' => 'image|mines:jpg,png,jpeg'
        ]);
        
        // if($request->has('thumbnail', $id)){
        //     $cast = Cast::find($id);

        //     $path = "thumnail/";
        //     File::delete($path . $cast->thumbnail);

        //     $namaThummbnail = time(). '.' . $request->thumbnail->extension();
        //     $request->thumbnail->move(public_path('thumbnail'), $namaThummbnail);

        //     $cast->nama = $request->nama;
        //     $cast->umur = $request->umur;
        //     $cast->bio = $request->bio;
            // $cast->thumnail = $namaThummbnail;

        //     $cast->save();

        // return redirect('/cast');
        // }else{
        //     $cast = Cast::find($id);
        //     $cast->nama = $request->nama;
        //     $cast->umur = $request->umur;
        //     $cast->bio = $request->bio;
        //     $cast->save();

        // return redirect('/cast');
        // }
        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->save();

        return redirect('/cast');
    }

    public function destroy($id){
        $cast = Cast::find($id);

        //     $path = "thumnail/";
        //     File::delete($path . $cast->thumbnail);

        $cast->delete();
        return redirect('/cast');
    }
}