@extends('layout.master')

@section('judul')
    Media Online
@endsection

@section('content')
    Sosial Media Developer
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    Benefit join di Media Online
    <ul>
        <li>Mendapatkan motivasi dari sesama pada Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    Cara bergabung ke Media Online
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a> </li>
        <li>Selesai</li>
    </ol>
@endsection