@extends('layout.master')

@section('judul')
    Sign Up Form
@endsection

@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <label>Firstname :</label>
        <br>
        <input type="text" name="firstname">
        <br><br>
        <label>Lastname :</label>
        <br>
        <input type="text" name="lastname">
        <br>
        <br>
        <label>Gender :</label>
        <br>
        <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other
        <br>
        <br>
        <label>Nationality :</label>
        <br>
        <br>
        <select name="nation">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
            <option value="4">Brunei</option>
        </select>
        <br>
        <br>
        <label>Language Spoken :</label>
        <br>
        <br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br>
        <br>
        <label>Biodata :</label>
        <br>
        <br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form>
@endsection