@extends('layout.master')

@section('judul')
    Detail Cast {{$cast->nama}}
@endsection

@section('content')

<h4>{{$cast->nama}}</h4>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

@endsection