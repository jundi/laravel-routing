@extends('layout.master')

@section('judul')
    Edit Cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama Anda">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Umur Anda">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Biodata</label>
                <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Biodata Anda">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
@endsection


    {{-- <div class="form-group">
        <label>Untuk Select</label>
        <select name="katergori_id" class="form-control">
            <option value="">Pilih category</option>
            @foreach ($kategori     as $item)
                @if ($item->id === $berita->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
                
            @endforeach
        </select>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div> --}}
   